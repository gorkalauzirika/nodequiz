module.exports = (grunt) ->
  grunt.initConfig
    pkg: grunt.file.readJSON "package.json"

    meta:
      endpoint: ''
      banner: """
        /* <%= pkg.name %> v<%= pkg.version %> - <%= grunt.template.today("m/d/yyyy") %>
           <%= pkg.homepage %>
           Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %> - Licensed <%= _.pluck(pkg.license, "type").join(", ") %> */

        """

    source:
      coffee: 
        server: [
          'source/model/*.coffee',
          'source/controller/*.coffee',
          'source/controller/gameModes/*.coffee',
          'source/server.coffee']
        adminserver: [
          'source/model/*.coffee',
          'source/controller/question.coffee',
          'source/adminServer.coffee']
        watch:
          ['source/view/watch/playing/*.coffee',
           'source/view/watch/waitingToPlay/*.coffee',
           'source/view/watch/*.coffee',
            'source/watch.coffee']
        index:
          [ 'source/view/index/*.coffee'
            'source/index.coffee']
        admin:
          [ 'source/view/admin/*.coffee',
            'source/admin.coffee']

      stylesheets: 'source/stylesheets/'

    coffee:
      compile: 
        files: 
          '<%= meta.endpoint %>server.debug.js': ['<%= source.coffee.server %>']
          '<%= meta.endpoint %>js/watch.debug.js': ['<%= source.coffee.watch %>']
          '<%= meta.endpoint %>js/index.debug.js': ['<%= source.coffee.index %>']
          '<%= meta.endpoint %>js/admin.debug.js': ['<%= source.coffee.admin %>']
          '<%= meta.endpoint %>adminserver.debug.js': ['<%= source.coffee.adminserver %>']

    uglify:
      options: compress: false, banner: "<%= meta.banner %>"
      coffee: 
        files: 
          '<%= meta.endpoint %>server.js': '<%= meta.endpoint %>server.debug.js'
          '<%= meta.endpoint %>js/watch.js': '<%= meta.endpoint %>js/watch.debug.js'
          '<%= meta.endpoint %>js/index.js': '<%= meta.endpoint %>js/index.debug.js'
          '<%= meta.endpoint %>js/admin.js': '<%= meta.endpoint %>js/admin.debug.js'
          '<%= meta.endpoint %>adminserver.js': '<%= meta.endpoint %>adminserver.debug.js'

    compass:
      dist: 
        options: 
          sassDir: "<%= source.stylesheets %>"
          cssDir:'<%= meta.endpoint %>css'
        
      

    watch:
      coffee:
        files: ["<%= source.coffee.server %>", "<%= source.coffee.watch %>", "<%= source.coffee.index %>", "<%= source.coffee.admin %>", "<%= source.coffee.adminserver %>" ]
        tasks: ["coffee"]
      stylesheets:
        files: ["<%= source.stylesheets %>*.scss"]
        tasks: ["compass"]


  grunt.loadNpmTasks "grunt-contrib-coffee"
  grunt.loadNpmTasks "grunt-contrib-uglify"
  grunt.loadNpmTasks "grunt-contrib-compass"
  #grunt.loadNpmTasks "grunt-contrib-concat"
  grunt.loadNpmTasks "grunt-contrib-watch"

  grunt.registerTask "default", ["coffee", "uglify", "compass"] #"concat"]

  ###
    concat:
      css:
        src: ['<%= source.css_core %>'],
        dest: '<%= meta.endpoint %>static/stylesheets/watch.css'
###
