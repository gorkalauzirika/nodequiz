# Installation

## Dependencies
+ NodeJS (http://nodejs.org/)
+ MongoDB (http://www.mongodb.org/)
+ Sass (http://sass-lang.com/)
+ CoffeeScript (http://coffeescript.org/)
+ Grunt (http://gruntjs.com/)

## Steps to follow

To install dependencies:

```npm install```

Run grunt tasks (Make sure grunt, sass and compass are installed)

```grunt```

Start MongoDB (Make sure MongoDB is installed and configured)

```mongod --dbpath="<path>"```

```mongo nodeQuiz```

Start game server

```node server.js```

Start admin server (if needed)

```node adminserver.js```

nodeQuiz runs in port 8080 by default
