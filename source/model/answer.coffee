class Answer
	constructor: (question, answer, player, time) ->
		@question = question
		@answer = answer
		@player = player
		@time = time

	parseForNotification: ->
		{question: @question.id, player: @player, time: @time}

exports.Answer = Answer