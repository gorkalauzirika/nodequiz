mongoose = require('mongoose')
class Question
	constructor: (id,question, choices, correct, category) ->
		@question = question
		@choices = choices
		@correct = correct
		@id = id;
		@category = category
		@answers = []
		@beginTimeStamp = null
		

	toArray: ->
		{id: @id, question: @question, choices: @choices, category: @category}

	formatForTimeout: ->
		formatedAnswers = []
		for answer in @answers
			formatedAnswers.push {answer: answer.answer, player: answer.player, time: answer.time}
		{id: @id, correct: @correct, answers: formatedAnswers}	

	addAnswer: (answer) ->
		@answers.push answer

	getAnswerByPlayerName: (playerName) ->
		for answer in @answers
			if answer.player.name is playerName then return answer
		undefined

	@getSchema: ->
		{question: String, choices: Array, correct: Number, category: String, random_value: Array}

	parseToMongo: ->
		{question: @question, choices: @choices, correct: @correct, category: @category, random_value: [Math.random(), 0]}

	saveToMongo: (callback) ->		
		q = new exports.QuestionModel do @parseToMongo
		q.save (err) ->
			if err
				throw "Error while saving to DB: " + err
			else
				if typeof(callback) is "function"
					callback q

exports.QuestionModel = mongoose.model 'Question', do Question.getSchema

exports.Question = Question

