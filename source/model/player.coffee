class Player
	constructor: (name, points) ->
		@name = name
		@points = points
		@ready = false

	addPoints: (points) ->
		@points = @points + points

	toArray: ->
		{name: @name, points: @points, ready: @ready}

exports.Player = Player