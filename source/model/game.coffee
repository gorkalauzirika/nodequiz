class Game 
	@WAITING_PLAYERS = 0
	@PLAYING = 1 

	@MODE_POINT_COLLECTOR = 0
	@MODE_TIME_ATTACK = 1
	@MODE_PASS_THE_PROBLEM = 2
	@MODE_ALL_IN = 3
	@MODE_POINT_THIEF = 4

	constructor: (settings,gameModes) ->
		@settings = {}
		@settings.questionAmount = settings.questionAmount || 20
		@settings.timePerQuestion = settings.timePerQuestion || 10
		@settings.showingAnswerTime = settings.showingAnswerTime || 5
		@settings.preRoundTime = settings.preRoundTime || 5

		@gameModes = gameModes
		@currentGameMode = 0

		@questionsLeft = @settings.questionAmount
		@players = []
		@currentQuestion = null
		@questions = []
		@status = @WAITING_PLAYERS
		
		

	nextQuestion: (question) ->
		@currentQuestion = question

	addPlayer: (player) ->
		@players.push player
		@playing++

	toArray: ->
		playerArray = []

		for player in @players
			playerArray.push do player.toArray
		
		{questionsLeft: @questionsLeft, totalQuestions: @settings.questionAmount, players: playerArray}

	readyToStart: ->
		if @players.length < 2 and @status is @WAITING_PLAYERS then return false
		for player in @players
			if not player.ready 
				return false
		return true

	setPlayerReady: (playerName, ready = true) ->
		player = @getPlayerByName playerName
		if player?
			player.ready = ready	

	getPlayerByName: (name) ->
		for player in @players
			if player.name is name then return player
		undefined

	nextRound: ->
		@currentGameMode++
		if @gameModes.length > @currentGameMode 			
			return @gameModes[@currentGameMode]
		else
			return null
exports.Game = Game