class WaitingPlayerView
	
	template = _.template($('#waiting-player-template').html())

	constructor: (model)->
		@model = model
		@.$el = $("<div class='question-box'></div>")
		
	render: ->
		@.$el.html(template(@model))
		@
		
	updatePlayer: (model) ->
		@model = model
		do @render
		if @model.ready then @.$el.addClass("correct")  
		@

	removePlayer: -> 


window.WaitingPlayerView = WaitingPlayerView