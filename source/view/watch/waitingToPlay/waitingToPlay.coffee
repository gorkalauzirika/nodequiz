class WaitingToPlayView
	constructor: ->
		@playerList = []
		@.$el = $("<section id='waiting-to-play'></section>")	
		@.$container = null

	render: ->
		@.$el.html('<p id="header-message">Esperando a jugadores para un nueva partida</p><div class="players"></div>')
		@.$container = @.$el.children(".players") 
		@

	addPlayer: (player) ->
		playerView = new WaitingPlayerView(player)
		@playerList[player.name] = playerView
		@.$container.append playerView.render().$el

	updatePlayer: (player) ->
		@playerList[player.name].updatePlayer(player)

	removePlayer: (player) ->

window.WaitingToPlayView = WaitingToPlayView

