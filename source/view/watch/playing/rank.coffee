class RankView
	
	constructor: ->
		@.$el = $("<section id='rank'></ul>")	
		@rankPlayers = []
	
	render: ->
		@.$el.html('')
		for player in @rankPlayers
			@.$el.append player.render().$el
		@

	addPlayer: (player) ->
		rankPlayerView = new RankPlayerView(player)
		@rankPlayers[player.name] = rankPlayerView
		@.$el.append rankPlayerView.render().$el

	updatePlayer: (player) ->
		if @rankPlayers[player.name]? then  @rankPlayers[player.name].updatePlayer(player) else @addPlayer player

	updatePlayers: (players) ->
		for player in players
			@updatePlayer player

	showAnswers: (answers,correct = null) ->
		for answer in answers
			@showAnswer answer, correct

	showAnswer: (answer,correct) ->
		if @rankPlayers[answer.player.name]? then @rankPlayers[answer.player.name].setAnswer(answer,correct)
		
	clearAnswers: ->
		for player in Object.keys(@rankPlayers)
			do @rankPlayers[player].cleanAnswer



window.RankView = RankView