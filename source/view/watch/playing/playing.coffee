class PlayingView
	constructor: ->
		@currentQuestion = null
		@.$el = $('article')
		@questionView = new QuestionView()
		@rankView = new RankView()	
		@roundView = new RoundView()

	render: ->
		@.$el.html('')
		#render Question
		@.$el.append @questionView.render().$el.hide()
		#render round
		@.$el.append @roundView.render().$el.hide()
		#render rank
		@.$el.append @rankView.render().$el
		

window.PlayingView = PlayingView
