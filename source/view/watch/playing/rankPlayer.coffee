class RankPlayerView
	template = _.template($('#rank-player-template').html())

	constructor: (model)->
		@model = model
		@model.answer = null
		@correctAnswer = null
		@.$el = $("<div class='rank-player'></div>")	
	
	render: ->
		@.$el.html(template(@model))
		@

	updatePlayer: (player) ->
		answer = @model.answer
		if player.points isnt @model.points
			sign = if player.points - @model.points > 0 then "+" else ""
			pointsClass =  if player.points - @model.points > 0 then "positive" else "negative"
			points = player.points - @model.points
			@.$el.children(".player-info").children("span").addClass(pointsClass).text("#{sign}#{points}")
			view = @
			setTimeout ( ->
			  view.$el.children(".player-info").children("span").fadeOut().removeClass(pointsClass).text(player.points).fadeIn()
			  view.model = player
			  view.model.answer = answer
			), 2000
		else
			@model = player
			@model.answer = answer

		@setAnswer @model.answer, @correctAnswer

	setAnswer: (answer,correctAnswer) ->
		@model.answer = answer
		@correctAnswer = correctAnswer
		numberToLetter = ["A","B","C","D"]
		#do @render
		#Show the answer
		el = @.$el.children ".answer"
		$(el).removeClass("correct").removeClass("wrong").removeClass("unknown")
		if @model.answer is null
			#We don't have the answer so hide it
			do $(el).hide
		else if @model.answer['answer']?
			$(el).addClass (if correctAnswer is @model.answer.answer then  "correct" else "wrong")
			$(el).text(numberToLetter[@model.answer['answer']])
			do $(el).show
		else
			#Show ? as we dont know the answer (while the question is still active)
			$(el).addClass "unknown"
			$(el).text("?")
			do $(el).show
		
	cleanAnswer: ->
		@setAnswer null

window.RankPlayerView = RankPlayerView