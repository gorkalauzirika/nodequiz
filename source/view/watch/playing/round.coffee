class RoundView
	template = _.template """
				<div class='details'>
	                <h1><%- name %></h1>
	                <ul>
	                <% _.each(tips, function(tip){ %>
		                <li><%- tip %></li>
		            <% }); %>
					</ul>
				</div>
                <img src='images/<%- image %>'/>"""

	constructor: (round)->
		@model = round || {}
		@.$el = $("<section class='round'></section>")

	render: ->
		if @model.name?
			@.$el.html(template(@model))
		else
			@.$el.html('')		
		@

	newRound: (round) ->
		@model = round 
		do @render
		do @.$el.show

	hide: ->
		do @.$el.hide

window.RoundView = RoundView
