class QuestionView
	template = _.template($('#question-template').html())

	constructor: (question)->
		@model = question || {}
		@.$el = $("<section class='question'></section>")	
	
	render: ->
		if @model.question?
			@.$el.html(template(@model))
		else
			@.$el.html('<p>Esperando siguiente pregunta</p>')		
		@

	newQuestion: (question, timer = true) ->
		@model = question
		do @render
		do @.$el.show
		el = @.$el.children("#timer").children("#slider")
		if timer	
			do el.show		
			$(el).css {"-webkit-transition-duration": "10s","-moz-transition-duration": "10s" ,"-o-transition-duration": "10s",	"transition-duration": "10s"}
			setTimeout ->
				$(el).css "width", "0px"
			, 1
		else 
			do el.hide

	markCorrect: (correct) ->
		i = 0
		for el in @.$el.children("div").children(".question-box")
			if correct is i then $(el).addClass("correct") else $(el).addClass("wrong")
			i++

	hide: ->
		do @.$el.hide		

window.QuestionView = QuestionView
