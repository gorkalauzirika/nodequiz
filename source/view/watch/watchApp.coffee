class WatchApp

	@PHASE_WAITING = 0
	@PHASE_PLAYING = 1
	constructor: ->
		@.$el = $('article')
		@currentContainer = null
		@currentPhase = WatchApp.PHASE_WAITING

	startLoading: ->
		#

	startWaitingPlayers: ->
		waitingToPlayView = new WaitingToPlayView()
		@.$el.html(waitingToPlayView.render().$el) 
		@currentContainer = waitingToPlayView
		@currentPhase = WatchApp.PHASE_WAITING

	startGame: ->
		playingView = new PlayingView()
		@.$el.html(playingView.render().$el) 
		@currentContainer = playingView
		@currentPhase = WatchApp.PHASE_PLAYING

	finishGame: ->
		alert 'Game finished'
		
window.WatchApp = WatchApp