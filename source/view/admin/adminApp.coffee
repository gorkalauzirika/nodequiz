class AdminApp

	constructor: (socket) ->
		@.$el = $('article')
		@socket = socket
		@listQuestion = new ListQuestionView()

	initialize: ->
		admin = @

		$("#filter-questions").on 'submit', ->
			do $(this).hide
			do $("#questions-loading").show
			filterArray = do $(@).serializeArray
			query = {}
			$.each filterArray, ->
				query[@name] = @value || ''
			admin.showList query
			false

		do @listQuestion.initialize		
	
	showList: (query) ->
		do @listQuestion.emptyQuestions
		@listQuestion.fetchQuestions query		

	emit: (type, message) ->
		@socket.emit type,message

	on: (type, callback) ->
		@socket.on type,callback


		
window.AdminApp = AdminApp