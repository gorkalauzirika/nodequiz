class ListQuestionView
	template = _.template($('#question-list-template').html())

	constructor: ->
		@model = null
		@.$el = $("section#question-list")
		@.$tbody = null
		@questions = []	

	initialize: ->
		do @bindEvents	
	
	render: ->
		@.$el.html(template)
		@.$tbody = $("section#question-list").children("table").children("tbody")
		for question in @questions
			qEl = question.render().$el
			@.$tbody.append qEl
		@

	fetchQuestions: (options) ->
		view = @
		app.socket.emit 'findQuestions', options

	addQuestion: (question) ->
		quest = new ListItemView(question)
		@questions.push quest
		@.$tbody.append quest.render().$el

	emptyQuestions: ->
		@questions = []

	bindEvents: ->
		view = @
		app.socket.on 'searchResults', (questions) ->
			do view.render
			view.questions = questions
			for question in questions
				view.addQuestion(question)
			do $("#questions-loading").hide
			do $("#filter-questions").show

window.ListQuestionView = ListQuestionView
