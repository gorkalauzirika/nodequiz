class ListItemView
	template = _.template($('#list-item-template').html())

	constructor: (question)->
		@model = question
		@.$el = $("<tr class='question'></tr>")	
		do @bindEvents
	
	render: ->
		if @model?
			@.$el.html(template(@model))
		@

	showDeleteConfirm: ->
		@.$el.children(".actions").children(".show-edit-buttons").hide()
		@.$el.children(".actions").children(".confirm-delete-buttons").show()

	hideDeleteConfirm: ->
		@.$el.children(".actions").children(".confirm-delete-buttons").hide()
		@.$el.children(".actions").children(".show-edit-buttons").show()		

	deleteQuestion: ->
		app.emit 'deleteQuestion', @model
		@.$el.fadeOut 500, ->
			do $(@).remove

	bindEvents: ->
		view = @

		@.$el.on 'click', '.delete-action', ->
			do view.showDeleteConfirm

		@.$el.on 'click', '.cancel-delete-action', ->
			do view.hideDeleteConfirm

		@.$el.on 'click', '.confirm-delete-action', ->
			do view.deleteQuestion


window.ListItemView = ListItemView
