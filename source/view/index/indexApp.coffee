class IndexApp

	@PHASE_WAITING = 0
	@PHASE_PLAYING = 1
	constructor: ->
		@.$el = $('article')
		@answerView = null
		@modalView = null
		@player = null  
		@currentPhase = IndexApp.PHASE_WAITING
		@socket = io.connect(document.URL) 

	initialize: ->
		app = @
		
		@socket.on 'gameStarted', (game) ->
			do app.startGame

		@socket.on 'newQuestion', (question) ->
			app.showNewQuestion question, 'question'

		@socket.on 'preQuestion', (question) ->
			app.showNewQuestion question, 'preQuestion'

		@socket.on 'postQuestion', (question) ->
			app.showNewQuestion question, 'postQuestion'

		@socket.on 'questionTimeout', (question) ->
			app.showModalMessage "Esperando a la siguiente pregunta"

		@socket.on 'gameFinished', ->
			app.showModalMessage "Juego terminado"

		@modalView = new ModalView()
		@.$el.append @modalView.render().$el

		$("#join-game").on 'submit', ->
			app.player = $(@).children('input').val()
			app.socket.emit 'joinGame', app.player
			do app.hideAll
			$('article').append($('#confirm-start').html())
			false
		
		$("article").on 'click', '#confirm-start-dialog button', ->
			app.socket.emit 'readyToStart', app.player
			#show waiting to start dialog
			app.showModalMessage "Esperando a otros jugadores"
			false

		##do @showJoinView

	showJoinView: ->
		joinView
		
	startGame: ->
		@currentPhase = IndexApp.PHASE_PLAYING
		@answerView = new AnswerView(@player,@socket)
		@.$el.append @answerView.render().$el

	showPreQuestion: (prequestion) ->
		do @hideAll
		if @answerView? then @answerView.newQuestion prequestion

	showNewQuestion: (question, type) ->
		do @hideAll
		if @answerView? then @answerView.newQuestion question, type

	showModalMessage: (message) ->
		do @hideAll
		@modalView.showMessage message

	hideAll: ->
		for it in @.$el.children('section')
			$(it).hide() 
		
window.IndexApp = IndexApp