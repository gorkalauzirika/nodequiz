class ModalView
	template = _.template($('#modal-template').html())

	constructor: ->
		@message = {}
		@.$el = $("<section id='modal'></section>")	
	
	render: ->
		if @message['message']?
			@.$el.html(template(@message))	
		@

	showMessage: (message) ->
		@message.message = message
		do @render
		do @.$el.show

window.ModalView = ModalView