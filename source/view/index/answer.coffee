class AnswerView
	template = _.template($('#answer-template').html())

	constructor: (playerName,socket)->
		@model = {}
		@socket = socket
		@playerName = playerName
		@.$el = $("<section id='answer'></section>")	
		@type = 'question' # Could be also 'preQuestion' and 'postQuestion'
		do @bindEvents
	
	render: ->
		if @model.question?
			@.$el.html(template(@model))
		else
			@.$el.html('<p>Esperando siguiente pregunta</p>')		
		@

	newQuestion: (question, type = 'question') ->
		@model = question
		@type = type
		do @render
		do @.$el.show

	answerQuestion: (answer) ->		
		console.log "Question #{@model.id} answered: " + answer
		if @type is 'question' 
			@socket.emit 'answer' , {question: @model.id, player: @playerName, answer: answer}
		else
			@socket.emit "#{@type}Answer" , {question: @model.id, player: @playerName, answer: answer}
		do @.$el.hide

	bindEvents: ->
		app = @
		@.$el.on 'click', "ul .answer-option", ->
			app.answerQuestion $(@).data('option')

window.AnswerView = AnswerView