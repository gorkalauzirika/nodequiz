express = require('express')
app = express()
server = app.listen(process.env.PORT || 8081)
io = require('socket.io').listen(server, { log: false })
fs = require 'fs'
exports.mongoose = require('mongoose')
exports.mongoose.connect process.env.MONGOHQ_URL || 'mongodb://localhost/nodeQuiz'
qCtrlr = new exports.QuestionController()

app.configure ->
  app.use express.bodyParser()
  app.use app.router

app.set 'view engine', 'jade'
app.set 'views', __dirname + '/source/view'

app.use("/css", express.static(__dirname + '/css'))
app.use("/js", express.static(__dirname + '/js'))
app.use("/images", express.static(__dirname + '/images'))

app.get '/', (req, res) -> 
    res.render 'admin/jade/index.jade'

app.get '/new', (req, res) -> 
  res.render 'admin/jade/newQuestion.jade', {question: {question:"",choices:["","","",""]},mode:"new"}
  
app.post '/new', (req, res) ->
  question = req.body
  q = new exports.Question(null,question.question, [question.choice0,question.choice1,question.choice2,question.choice3], question.correct, question.category)
  q.saveToMongo (savedQ) ->
    if(savedQ.id?)
      res.redirect "/edit/#{savedQ.id}"
    else
      res.render 'admin/jade/newQuestion.jade', {question: q,mode:"new", error: "No se pudo guardar la pregunta"}

app.get '/edit/:id' , (req, res) ->
  qCtrlr.findQuestions {_id:req.params.id}, (questions) ->
    if questions.length is 1
      res.render 'admin/jade/newQuestion.jade', {question: questions[0], mode:"edit"}
    else
      res.status(404).send('Question not found');

app.post '/edit/:id' , (req, res) ->
  question = req.body
  if req.params.id? 
      question.choices = [question.choice0,question.choice1,question.choice2,question.choice3]
      qCtrlr.updateQuestion req.params.id, question
      question._id = req.params.id
      res.render 'admin/jade/newQuestion.jade', {question: question, mode:"edit",saved:true}


app.get '/export', (req, res) -> 
  questions = qCtrlr.findQuestions {}, (questions) ->
    res.writeHead(200, { 'Content-Type': 'application/json;charset=UTF-8'})
    res.write(JSON.stringify(questions),"utf8")
    res.end()

app.get '/import', (req, res) ->
  res.render 'admin/jade/import.jade'

app.post '/import/json', (req, res) ->
  fs.readFile req.files.jsonfile.path, 'utf8', (err, data) ->
    if err
      console.log('Error: ' + err)
      return
    questions = JSON.parse data
    for question in questions
      if question._id?        
        qCtrlr.updateQuestion question._id, question
    res.redirect('/');

   
io.sockets.on 'connection',  (socket) ->
  console.log 'New connection'

  socket.on 'deleteQuestion', (question) ->
    if question._id? 
      qCtrlr.deleteQuestion question._id
      console.log "Question #{question._id} deleted"

  socket.on 'findQuestions', (query) ->
    if query.category? and query.category is "all"
      delete query.category
    query.question = new RegExp(query.question, "i")
    questions = qCtrlr.findQuestions query, (questions) ->
      socket.emit 'searchResults', questions


