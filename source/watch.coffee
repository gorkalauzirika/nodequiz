$(document).ready ->
	app = new WatchApp()

	do app.startWaitingPlayers
	#Load the app
		#Ask for game info
			#In case of waiting players, retrieve players
			#In case of ingame, retrieve game status and current question
		#Start listening to sockets
	socket = io.connect("http://localhost:8080")

	socket.on 'playerJoined', (player) ->
		console.log player.name + ' joined and is ' + (if not player.ready then 'not ') + 'ready'
		#If game not in progress show in the list
		app.currentContainer.addPlayer player
		#If game in progress show message in the bottom

	socket.on 'playerReady', (player) ->
		console.log player + ' is ready'
		app.currentContainer.updatePlayer player

	socket.on 'gameStarted', (game) ->
		console.log 'Game started ' + game
		#Show option buttons
		do app.startGame

	socket.on 'preRound', (round) ->
		console.log 'preRound'
		do app.currentContainer.rankView?.clearAnswers
		do app.currentContainer.questionView?.hide
		app.currentContainer.roundView?.newRound round

	socket.on 'preQuestion', (question) ->
		console.log 'preQuestion'
		do app.currentContainer.roundView?.hide
		app.currentContainer.questionView?.newQuestion question, false

	socket.on 'newQuestion', (question) ->
		do app.currentContainer.roundView?.hide
		console.log 'newQuestion: ' + question.question
		if app.currentPhase isnt WatchApp.PHASE_PLAYING then do app.startGame
		#Update question info
		app.currentContainer.questionView?.newQuestion question
		app.currentContainer.rankView?.clearAnswers()

	socket.on 'questionTimeout', (question) ->
		console.log 'Question timeout: Correct answer is: ' + question.correct
		#Show correct answer
		app.currentContainer.questionView?.markCorrect question.correct
		app.currentContainer.rankView?.showAnswers question.answers, question.correct

	socket.on 'rankChanged', (players) ->
		console.log 'Game status changed'
		#Update player info
		app.currentContainer.rankView?.updatePlayers players

	socket.on 'answered', (answer) ->
		app.currentContainer.rankView?.showAnswer answer

	socket.on 'postRound', (data) ->
		console.log 'preRound'

	socket.on 'gameFinished', (game) ->
		console.log 'gameFinished'
		#Show standings
		app.finishGame game
	

