class BetYourPointsMode extends exports.GameMode 

	modeName: "Apuesta tus puntos"
	tips: ["Selecciona cuantos puntos apostar", "Si aciertas, ganaras los puntos apostados", "Si fallas, los perderas"]
	image: "timeattack.png"

	constructor: (socket)->
		super socket
		@betChoices = [50,100,150,200]
		@playerBets = {}
		@betsPlaced = 0

	preQuestions: ->
		@betsPlaced = 0
		@game.currentQuestion = @game.questions[@game.settings.questionAmount - @game.questionsLeft]
		@game.questionsLeft = @game.questionsLeft - 1
		console.log "Pre Question: #{@game.currentQuestion.question}"
		#Send bet message
		@socket.emit 'preQuestion', 
			id: @game.currentQuestion.id
			question:"¿Cuantos puntos deseas apostar para una pregunta de #{@game.currentQuestion.category}?"
			choices: @betChoices

	handlePreQuestionAnswer: (answer) ->
		super answer

		if answer.player? and answer.question is @game.currentQuestion.id
			@playerBets[answer.player] = if answer.answer? and answer.answer < @betChoices.length then @betChoices[answer.answer] else 0
			@betsPlaced++
			if @game.players.length is @betsPlaced 
				do @showQuestion

	addPoints: ->
		for answer in @game.currentQuestion.answers
			if answer.answer is @game.currentQuestion.correct
				answer.player.points += @playerBets[answer.player.name]
			else
				answer.player.points -= @playerBets[answer.player.name]


exports.BetYourPointsMode = BetYourPointsMode 
