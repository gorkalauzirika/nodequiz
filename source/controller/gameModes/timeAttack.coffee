class TimeAttackMode extends exports.GameMode 
	modeName: "Contrareloj"
	tips: ["Responde cuanto antes", "El valor de los aciertos depende de la rapidez", "Los errores no penalizan"]
	image: "timeattack.png"

	addPoints: ->
		for answer in @game.currentQuestion.answers
			if answer.answer is @game.currentQuestion.correct
				answer.player.points += Math.round((@game.settings.timePerQuestion * 1000 - answer.time) / (@game.settings.timePerQuestion * 1000) * 200)

exports.TimeAttackMode = TimeAttackMode
