mongoose = require('mongoose')
class QuestionController
	findQuestions: (options, callback) ->
		q = exports.QuestionModel.find(options).limit(500)
		q.execFind (err,questions) ->
			callback questions

	updateQuestion: (id, question) ->
		exports.QuestionModel.findOne { "_id": id }, (err, q) ->
			if q?
				q.question = question.question
				q.choices = question.choices
				q.category = question.category
				q.correct = question.correct
				do q.save
			else 
				console.log "Question #{id} does not exist creating new one"
				if question.question? and question.choices? and question.correct? and question.category?
					newQ = new exports.Question(null,question.question, question.choices, question.correct, question.category)
					do newQ.saveToMongo

	deleteQuestion: (id) ->
		exports.QuestionModel.findOne { "_id": id }, (err, q) ->
			if q? 
				do q.remove
 exports.QuestionController = QuestionController