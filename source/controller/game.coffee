{EventEmitter} = require 'events'
mongoose = require('mongoose')
class GameController extends EventEmitter

	constructor: (socket,game) ->
		@socket = socket
		@game = game

		ctrlr = @
		socket.on 'connection',  (socket) ->
			console.log 'New connection'

			socket.on 'joinGame', (playerName) ->
				#Check if username not repeated
				console.log playerName + ' joined the game'
				player = new exports.Player(playerName,0)
				ctrlr.game.addPlayer(player)
				ctrlr.socket.emit 'playerJoined', do player.toArray

			socket.on 'readyToStart', (playerName) ->
				ctrlr.readyToStart playerName

			socket.on 'answer', (data) ->
				ctrlr.answerQuestion data

			socket.on 'preQuestionAnswer', (data) ->
				console.log 'preQuestionAnswer'

			socket.on 'leaveGame', (data) ->
				console.log data.player + ' left the game (TODO)'


	startGame: ->
		@game.status = exports.Game.PLAYING
		ctrlr = @
		@loadQuestions ->
			ctrlr.socket.emit 'gameStarted', do ctrlr.game.toArray
			ctrlr.socket.emit 'rankChanged', ctrlr.game.players
			ctrlr.nextRound ctrlr.game.gameModes[0]

	readyToStart: (playerName) ->
		console.log playerName + ' is ready to start'
		@game.setPlayerReady playerName

		@socket.emit 'playerReady', do @game.getPlayerByName(playerName).toArray

		if do @game.readyToStart then do @startGame

	nextRound: (gameMode) ->
		console.log 'New round'
		@game.questionsLeft = @game.settings.questionAmount
		@game.questions = []
		ctrlr = @
		@loadQuestions ->
			gameMode.startRound ctrlr.game

			gameMode.on 'roundFinished', ->
				next = do ctrlr.game.nextRound
				if next? then ctrlr.nextRound next else do ctrlr.finishGame


	loadQuestions: (callback) ->
		ctrlr = @
		exports.QuestionModel.findOne {random_value: {$near: [Math.random(), 0] }} , (error, result) ->
			if not error
				ctrlr.game.questions.push new exports.Question(result.id,result.question,result.choices,result.correct,result.category)
				if ctrlr.game.questions.length < ctrlr.game.settings.questionAmount 
					ctrlr.loadQuestions callback
				else 
					do callback
			else
				console.log error	

	answerQuestion: (data) ->
		if @game.currentQuestion.id is data.question
			#Get player data
			player = @game.getPlayerByName(data.player)
			if @game.currentQuestion.beginTimeStamp?
				now = new Date()
				time = do now.getTime - do @game.currentQuestion.beginTimeStamp.getTime
			else time = null
			#Store the answer
			answer = new exports.Answer(@game.currentQuestion,data.answer,player,time)
			@game.currentQuestion.addAnswer answer
			console.log 'New answer: ' + player + ' answered ' + data.answer + ' in question ' + data.question + ' in ' + time + 'ms'
			@socket.emit 'answered', do answer.parseForNotification	

	finishGame: (game) ->
		@socket.emit 'gameFinished', do @game.toArray
		for player in @game.players
			player.ready = false
		@game.answers = []
		@game.currentQuestion = null
		@game.questions = []
		@game.status = @game.WAITING_PLAYERS
		@game.questionsLeft = @game.settings.questionAmount
		console.log 'Game finished'

exports.GameController = GameController		