{EventEmitter} = require 'events'
class GameMode extends EventEmitter
	modeName: "Suma puntos"
	tips: ["Todos deben responder a la pregunta", "Los aciertos valen 100 puntos", "Los errores no penalizan"]
	image: "pointBuilder.jpg"

	constructor: (socket) ->
		@game = null
		@socket = socket
		mode = @
		@socket.on 'connection',  (socket) ->
			socket.on 'preQuestionAnswer', (answer) ->
				mode.handlePreQuestionAnswer answer

	startRound: (game) ->
		@game = game
		do @preRound

	preRound: ->
		@socket.emit 'preRound', {name: @modeName, tips: @tips, image: @image}
		ctrlr = @
		setTimeout ( ->
			do ctrlr.preQuestions
		), @game.settings.preRoundTime * 1000
		

	preQuestions: ->
		@game.currentQuestion = @game.questions[@game.settings.questionAmount - @game.questionsLeft]
		@game.questionsLeft = @game.questionsLeft - 1
		console.log "Pre Question: #{@game.currentQuestion.question}"

		do @showQuestion

	handlePreQuestionAnswer: (answer) ->
		console.log "Handle pre question answer"

	showQuestion: ->
		console.log "Show Question: #{@game.currentQuestion.question}"
		@game.currentQuestion.beginTimeStamp = new Date()
		@socket.emit 'newQuestion', @game.currentQuestion  
		#Add question timeout
		ctrlr = @
		setTimeout ( ->
		  do ctrlr.postQuestion 
		), @game.settings.timePerQuestion * 1000

	postQuestion: ->
		console.log "Post Question: #{@game.currentQuestion.question}"
		#Show updated points and correct answer
		@socket.emit 'questionTimeout', do @game.currentQuestion.formatForTimeout
		#Add points
		do @addPoints
		@socket.emit 'rankChanged', @game.players
		#Check if last question
		ctrlr = @
		setTimeout ( ->
		  if ctrlr.game.questionsLeft > 0 then do ctrlr.preQuestions else do ctrlr.postRound 
		), @game.settings.showingAnswerTime * 1000

	handlePostQuestionAnswer: (answer) ->
		console.log "Handle post question answer"

	postRound: ->
		@socket.emit 'roundFinished'
		@emit 'roundFinished'

	addPoints: ->
		for answer in @game.currentQuestion.answers
			if answer.answer is @game.currentQuestion.correct
				answer.player.points += 100

exports.GameMode = GameMode			
