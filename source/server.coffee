express = require('express')
app = express()
server = app.listen(8080)
io = require('socket.io').listen(server, { log: false })
exports.mongoose = require('mongoose')
exports.mongoose.connect 'mongodb://localhost:27017/nodeQuiz'

app.use("/css", express.static(__dirname + '/css'))
app.use("/js", express.static(__dirname + '/js'));
app.use("/images", express.static(__dirname + '/images'));
app.use("/sound", express.static(__dirname + '/sound'));

game = new exports.Game({showingAnswerTime:5,questionAmount:10}, [new exports.BetYourPointsMode(io.sockets)])
gameCtrl = new exports.GameController(io.sockets,game)

app.get '/', (req, res) -> 
  res.sendfile(__dirname + '/index.html')
  console.log 'Index page sent'

app.get '/watch', (req, res) ->
  res.sendfile(__dirname + '/watch.html')

console.log 'Server started'
